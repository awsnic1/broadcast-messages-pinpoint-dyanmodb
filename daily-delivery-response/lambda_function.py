import json
import boto3
from boto3.dynamodb.conditions import Key

def lambda_handler(event, context):
    print('Event: ', event)
    try:
        for record in event['Records']:
            print('Record: ', record)
            if 'originationNumber' in record['Sns']['Message']:
                handle_response(record['Sns']['Message'], record['Sns']['Timestamp'])
    except Exception as e:
        print('Event exception: ', e)
        return
        
def handle_response(message, timestamp):
    print('Handling response message: ', message)
    messageObj = json.loads(message)
    print('Message object: ', messageObj)
    
    try:
        originationNumber = messageObj['originationNumber']
        messageBody = messageObj['messageBody']
    except Exception as e:
        print('Message exception: ', e)
        return
    
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('demo-route-delivery')
    
    try:
        query_results = table.query(
            IndexName='customerMobile-index',
            KeyConditionExpression=Key('customerMobile').eq(originationNumber)
        )
    except Exception as e:
        print('DynamoDB query exception: ', e)
        return
    
    print('DynamoDB query results: ', query_results)
    
    delivery_id = query_results['Items'][0]['delivery_id']
    
    print('Delivery ID: ', delivery_id)
    
    try:
        response = table.update_item(
            Key={
                'delivery_id': delivery_id,
            },
            UpdateExpression='set responseCustomer = :r, responseTimestamp = :t',
            ExpressionAttributeValues={
               ':r': messageBody,
               ':t' : timestamp,
            },
            ReturnValues="UPDATED_NEW"
        )
        print('DynamoDB update response: ', response)
    except Exception as e:
        print('DynamoDB exception: ', e)
        return