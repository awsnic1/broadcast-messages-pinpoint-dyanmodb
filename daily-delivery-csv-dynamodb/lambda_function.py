import json
import csv
import boto3
import time 

#Expire time
expiryTimestamp = int(time.time() + 72000 )
print('Expiry', expiryTimestamp)
creationTimestamp = int(time.time())
print('Create', creationTimestamp)


def lambda_handler(event, context):
    region = 'us-east-1'
    record_list = []
    
    try:
        s3 = boto3.client('s3')
        dynamodb = boto3.client('dynamodb', region_name = region)
        
        bucket = event['Records'][0]['s3']['bucket']['name']
        key = event['Records'][0]['s3']['object']['key']
        
        #confirm code reads bucket name and key in test
        print('Bucket: ', bucket  , ' Key: ', key)
        
        ### Now read csv file using boto3
        #first get file from s3
        csv_file= s3.get_object(Bucket = bucket, Key = key)
        #construct list
        record_list = csv_file['Body'].read().decode('utf-8').split('\n')
        #Now use csv imported to read the csv 
        csv_reader = csv.reader(record_list, delimiter = ',' ,  quotechar = '"')
        #loop through each row/record
        
        for row in csv_reader:
            delivery_id = row[0]
            customerName = row[1]
            customerAddress = row[2]
            customerMobile = row[3]
            customerEmail = row[4]
            responseCustomer = row[5]
            responseTimestamp = row[6] 
            

            print('Deliver Id: ', delivery_id, 'Customer Name: ', customerName, 'Customer Address: ', customerAddress, 'Customer Mobile: ', customerMobile, 'Customer Email: ', customerEmail)
            add_to_db = dynamodb.put_item(
                TableName = 'demo-route-delivery',
                Item = {
                    'delivery_id' : {'S': str(delivery_id)},
                    'customerName' : {'S': str(customerName)},
                    'customerAddress' : {'S': str(customerAddress)},
                    'customerMobile' : {'S': str(customerMobile)},
                    'customerEmail' : {'S': str(customerEmail)},
                    'creationTimestamp' : {'N' : str(creationTimestamp)},
                    'expiryTimestamp' : {'N' : str(expiryTimestamp)},
                    'responseCustomer' : {'S': str(responseCustomer)},
                    'responseTimestamp' : {'S': str(responseTimestamp)}
                    
                })
            
                
    except Exception as e: 
        print(str(e))
        
    return {
        'statusCode': 200,
        'body': json.dumps('CSV to DynamoDB success!')
    }
