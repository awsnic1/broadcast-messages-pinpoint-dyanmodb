from pinpoint_sms import *
from global_var import *
from decimal import Decimal

def lambda_handler(event, context):
    print('Event: ', event);
    print('Event record: ', event['Records']);
    try:
        for record in event['Records']:
            print('Event name: ', record['eventName'])
            if record['eventName'] == 'INSERT':
                handle_insert(record)
    except Exception as e:
        print(e)
        
def handle_insert(record):
    print('Handling insert for record ', record);
    delivery_id = record['dynamodb']['Keys']['delivery_id']['S']
    print('delivery_id: ', delivery_id);
    newImage = record['dynamodb']['NewImage']
    print('newImage: ', newImage);
    customerName = newImage['customerName']['S']
    
    customerAddress = newImage['customerAddress']['S']
    
    customerMobile = newImage['customerMobile']['S']
    
    customerEmail = newImage['customerMobile']['S']
    
    print('Name, address, mobile, email, response ', customerName, customerAddress, customerMobile, customerEmail)
    
    message = "Schwan’s Home Delivery is visiting your area tomorrow. Would you like us to visit you? Reply YES to confirm, NO to reschedule."
    print("Sending message to", customerMobile)
    send_sms_status = sendSMS(region,origination_number,customerMobile,message,application_id,message_type,sender_id)