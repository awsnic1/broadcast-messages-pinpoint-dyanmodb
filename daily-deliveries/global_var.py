"""
This is global module to assign the value of variables which is not coming from DynamoDB
"""
# The Amazon Pinpoint project/application ID to use when you send this message.
# Make sure that the SMS channel is enabled for the project or application
# that you choose.
application_id = "feb2d1e69d0e4ae9a07ee9f21f5087d1"

# The type of SMS message that you want to send. If you plan to send
# time-sensitive content, specify TRANSACTIONAL. If you plan to send
# marketing-related content, specify PROMOTIONAL.
message_type = "TRANSACTIONAL"

# The registered keyword associated with the originating short code.
registered_keyword = "KEYWORD_412889662808"

#Note sender_id is not suppported in US
sender_id = "MY-DELIVERY"

# The AWS Region that you want to use to send the message. For a list of
# AWS Regions where the Amazon Pinpoint API is available, see
# https://docs.aws.amazon.com/pinpoint/latest/apireference/
region = "us-east-1"

# The phone number or short code to send the message from. The phone number
# or short code that you specify has to be associated with your Amazon Pinpoint
# account. For best results, specify long codes in E.164 format.
origination_number = "+18777630713"

# The character encoding that you want to use for the subject line and message
# body of the email.  
charset = "UTF-8"
